" ----------------------------------------
" |                                      |
" |                                      |
" |                                      |
" |        Owen's (neo)vim config        |
" |                                      |
" |                                      |
" |                                      |
" ----------------------------------------
"

" ----------------
" |              |
" |    Basics    |
" |              |
" ----------------

filetype plugin on
filetype indent on
set colorcolumn=80
set lazyredraw
set magic
set showmatch
set noerrorbells
set novisualbell
set number
set exrc
set secure
set nobackup
set noswapfile
set nowb
set autoread
set noexpandtab
set relativenumber
set smarttab
set shiftwidth=4
set tabstop=4
set ai
set si
set scrolloff=10
set bg=dark
set wrap
let g:mapleader = "\\"
let g:maplocalleader = ","
set statusline=%=&P\ %f\ %m
set fillchars=vert:\ ,stl:\ ,stlnc:\
set cursorline
set cursorcolumn
hi ExtraWhitespace ctermfg=red guifg=red
hi NonText ctermfg=darkgray guifg=darkgray
hi CursorColumn ctermbg=black guibg=black
hi CursorLine ctermbg=black guibg=black cterm=NONE gui=NONE
let g:table_mode_corner='|'
match ExtraWhitespace /\s\+\%#\@<!$/


" -----------------
" |               |
" |    Plugins    |
" |               |
" -----------------

call plug#begin()
Plug '414owen/vim-headings'
Plug 'easymotion/vim-easymotion'
Plug 'godlygeek/tabular'
Plug 'idris-hackers/idris-vim'
Plug 'junegunn/vim-easy-align'
Plug 'junegunn/vim-plug'
Plug 'mattn/emmet-vim'
Plug 'mxw/vim-jsx'
Plug 'pangloss/vim-javascript'
Plug 'rust-lang/rust.vim'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'tpope/vim-speeddating'
Plug 'tpope/vim-vinegar'
Plug 'dhruvasagar/vim-table-mode'
Plug 'mxw/vim-prolog'
Plug 'ElmCast/elm-vim'
Plug 'luochen1990/rainbow'
call plug#end()


" ------------------------
" |                      |
" |    Plugin Options    |
" |                      |
" ------------------------

let g:jsx_ext_required = 0
let g:NERDSpaceDelims = 1
let g:NERDCompactSexyComs = 1
let g:NERDDefaultAlign = 'left'
let g:NERDCommentEmptyLines = 1
let g:NERDTrimTrailingWhitespace = 1
let g:rainbow_active = 1
set list
set listchars=precedes:«,extends:»,eol:⤸,tab:›\ ,trail:·


" ---------------------
" |                   |
" |    Keybindings    |
" |                   |
" ---------------------

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

if has('nvim')
	" Use <esc> to escape terminal mode
	tnoremap <Esc> <C-\><C-n>
endif

" Fix line endings
nnoremap <leader>l :%s/\r/\r/g

" Yank to clipboard
nnoremap <leader>y  "+y
nnoremap <leader>ya mkgg"+yG'k
inoremap jk <esc>
inoremap kj <esc>

" Indent with saved cursor
nnoremap <leader>= mkgg=G'k

" --------------
" |  Snippets  |
" --------------

nnoremap <leader>srust :read ~/.config/nvim/snippets/rust<esc>jA
nnoremap <leader>sjava :read ~/.config/nvim/snippets/java<esc>4j02wdei
nnoremap <leader>sc-- :read ~/.config/nvim/snippets/c<esc>5jA
nnoremap <leader>scmm :read ~/.config/nvim/snippets/c<esc>5jA
nnoremap <leader>sco :read ~/.config/nvim/snippets/co<esc>12jA
nnoremap <leader>sc++ :read ~/.config/nvim/snippets/c++<esc>3jA
nnoremap <leader>scpp :read ~/.config/nvim/snippets/c++<esc>3jA
nnoremap <leader>shtml :read ~/.config/nvim/snippets/html<esc>8jA
nnoremap <leader>sscala :read ~/.config/nvim/snippets/scala<esc>3jA
nnoremap <leader>sreact :read ~/.config/nvim/snippets/react<esc>2j3whi

" -------------
" |  Headers  |
" -------------

nmap <leader>h1 :call Heading(1)<CR>
nmap <leader>h2 :call Heading(2)<CR>
nmap <leader>h3 :call Heading(3)<CR>
nmap <leader>h4 :call Heading(4)<CR>
nmap <leader>h5 :call Heading(5)<CR>
nmap <leader>h6 :call Heading(6)<CR>
nmap <leader>h7 :call Heading(7)<CR>
nmap <leader>h8 :call Heading(8)<CR>
nmap <leader>h9 :call Heading(9)<CR>
nmap <leader>dh1 :call UnHeading(1)<CR>
nmap <leader>dh2 :call UnHeading(2)<CR>
nmap <leader>dh3 :call UnHeading(3)<CR>
nmap <leader>dh4 :call UnHeading(4)<CR>
nmap <leader>dh5 :call UnHeading(5)<CR>
nmap <leader>dh6 :call UnHeading(6)<CR>
nmap <leader>dh7 :call UnHeading(7)<CR>
nmap <leader>dh8 :call UnHeading(8)<CR>
nmap <leader>dh9 :call UnHeading(9)<CR>
nmap <leader>m :!make<cr>
