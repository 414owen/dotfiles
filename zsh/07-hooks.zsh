function command_not_found_handler() {
	>&2 echo "${fg[yellow]}I can't find that${reset_color} :'("
	pkgs="$(run_if_present pkgfile "${1}")"
	if ! [ -z "${pkgs}" ]; then
		echo "\n${fg[blue]}It is provided by${reset_color}:"
		echo "${pkgs}"
	fi
}

function zshexit() {
    clear
}

function preexec() {
	echo "\n # ${fg[green]}Running${reset_color}: '${fg[blue]}${1}${reset_color}'\n"
	__LAST_COMMAND=${1}
	__LAST_COMMAND_TIME=$(date +%s%3N)
}

function precmd() {
	if ! [ -z "${__LAST_COMMAND}" ]; then
		elapsed_time="$(format_millis $(( $(date +%s%3N) - ${__LAST_COMMAND_TIME} )))"
		echo "\n # Command '${fg[blue]}${__LAST_COMMAND}${reset_color}' finished in ${elapsed_time}"
		__LAST_COMMAND=""
	fi
}
