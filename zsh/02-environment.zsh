# ZSH
export KEYTIMEOUT=1

# Default Clients
export EDITOR="$(first_available ${editors})"
export EBOOK_READER="$(first_available ${ebook_readers})"
export GIT="$(first_available ${git_clients})"

# Locale
export LANG="en_GB.UTF-8"
export LC_COLLATE=${LANG}
export LC_CTYPE=${LANG}
export LC_MESSAGES=${LANG}
export LC_MONETARY=${LANG}
export LC_NUMERIC=${LANG}
export LC_TIME=${LANG}
export LC_ALL=${LANG}
export LANGUAGE=${LANG}
export LESSCHARSET="utf-8"

# Coreutils
export BLOCK_SIZE="human-readable"

# Path
export PATH="${PATH}:${HOME}/.local/bin"
