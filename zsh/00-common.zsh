
# In order of preference
editors=(
"nvim"
"vim"
"vim.tiny"
"emacs"
"xi"
"vi"
"nano"
"pico"
)

git_clients=(
"hub"
"git"
)

ebook_readers=(
"FBReader"
"ebook-viewer"
)

text_formats=(
"c"
"cpp"
"css"
"csv"
"h"
"hpp"
"hs"
"html"
"idr"
"java"
"js"
"json"
"rb"
"rs"
"toml"
"txt"
"xml"
)

ebook_formats=(
"epub"
"mobi"
)
