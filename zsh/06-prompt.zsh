autoload -Uz colors && colors
autoload -Uz compinit && compinit

spacing="%{${reset_color}%} | "

function get_pwd {
  echo "${PWD/$HOME/~}"
}
function git_branch {
	echo $(git symbolic-ref HEAD | cut -d'/' -f3)
}

# Takes a command to expand and the foreground colour
function show_if {
	val=$(${1} 2>/dev/null)
	if ! [[ -z "${2}" ]]; then
		fore="%{${fg[${2}]}%}"
	else
		fore="%{${reset_color}%}"
	fi
	if ! [[ -z "${val}" ]]; then
		echo "${spacing}${fore}${val}"
	fi
}

function bracket_text {
	echo "[%{${fg[${2}]}%}${1}%{${reset_color}%}]"
}

setopt prompt_subst
PROMPT='
%{${fg[green]}%}${USER}${spacing}%{${fg[blue]}%}%m${spacing}%{${fg[yellow]}%}$(get_pwd)
%{${fg[red]}%} λ: %{${reset_color}%}'

# NORMAL_MODE="$(bracket_text normal red)"
NORMAL_MODE="%{${fg[blue]}%}normal%{${reset_color}%}"
# INSERT_MODE="$(bracket_text insert blue)"
INSERT_MODE="%{${fg[green]}%}insert%{${reset_color}%}"
RPROMPT='${${KEYMAP/vicmd/${NORMAL_MODE}}/(main|viins)/${INSERT_MODE}}$(show_if git_branch blue)'

function zle-line-init zle-keymap-select {
    zle reset-prompt
}

zle -N zle-line-init
zle -N zle-keymap-select
