# repeat string ${1} ${2} times
function rep() {
	printf "${1}%.0s" {1..${2}}
}

function first_available() {
	# one editor is enough.
	for i in ${@}; do
		if $(type "${i}" 2>&1 >/dev/null); then
			echo "${i}"
			break
		fi
	done
}

function alias_all() {
	for i in ${(P)${1}}; do
		if ! [[ "${i}" = "${2}" ]]; then
			alias ${3} ${i}="${2}"
		fi
	done
}

function suffix_alias_all() {
	alias_all ${1} ${2} -s
}

function format_millis() {
	echo "$(( ${1} / 1000 ))s $(( ${1} % 1000 ))ms"
}

function run_if_present() {
	if $(type "${1}" 2>&1 >/dev/null); then
		${@}
	fi
}

function heading() {
	horiz="━"
	topleft="┏"
	bottomleft="┗"
	toplength=10
	bottomlength=40
	echo "\n${topleft}$(rep ${horiz} ${toplength})" ${@}
	echo "${bottomleft}$(rep ${horiz} ${bottomlength})\n"
}

# read markdown files like manpages
function md() {
	pandoc -s -f markdown -t man "$*" | man -l -
}

# nullpointer url shortener
function short() {
	curl -F"shorten=$*" https://0x0.st
}

# xres and yres are aliases
function res() {
	xrandr --current | grep '*' | uniq | awk '{print $1}'
}

function record() {
	ffmpeg -f x11grab -s $(res) -an -r 16 -loglevel quiet -i :0.0 -b:v 5M -y ${@}
}

function aliadd() {
	echo "alias ${1}='${2}'" >> ~/.zshrc
}

function txt() {
	cat /dev/stdin > ${1}
}

function txte() {
	tail -n -10 ${1} && cat /dev/stdin >> ${1}
}

function v() {
	pactl set-sink-volume 0 "${1}%"
}

# No output == yes (useful with test -z)
function yn() {
	read REPLY\?"${1} (y/n): "
	if [ "${REPLY}" = "y" ]; then
		echo "${REPLY}"
	fi
}
